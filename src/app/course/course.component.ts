import { CoursesService } from './../courses.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  title = 'List of courses';
  courses: string[];
  day = new Date();
  imageUrl = 'https://via.placeholder.com/150';
  isActive = false;
  email = 'example@gmail.com';

  constructor(service: CoursesService) {
    this.courses = service.getCourses();
    setInterval(() => this.updateDate(), 1000);
  }

  ngOnInit(): void {
  }

  updateDate(): void {
    this.day = new Date();
  }

  onSave($event: Event): void {
    console.log('Saved', $event);
    console.log(this.day);
  }

  onKeyUp(): void {
    console.log(this.email);
  }
}
